package sheridan;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {
	
	private static final String regex = "[a-zA-Z]{1}[a-zA-Z0-9]{5,}";

	public static boolean isValidLoginName( String loginName ) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(loginName);
		
		return matcher.matches();
	}
}
