package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Harpreet" ) );
	}
	
	@Test
	public void testIsValidLoginEcecption( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1ramses" ) );
	}
	
	@Test
	public void testIsValidLoginBoundryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "rams3s" ) );
	}
	
	@Test
	public void testIsValidLoginBoundryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1ramses" ) );
	}
	
	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginLengthEception( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "r" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "ramse" ) );
	}

}
